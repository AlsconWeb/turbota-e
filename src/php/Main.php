<?php
/**
 * Initial theme file.
 *
 * @package iwpdev/turbota
 */

namespace IWPDEV\Turbota;

/**
 * Main class file.
 */
class Main {

	/**
	 * Theme version
	 */
	public const T_THEMES_VERSION = '1.0.0';

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();
		
		new GoogleDrive();
		new CTF7_VPO_Form();
	}

	/**
	 * Init hook and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scrip' ] );
		add_filter( 'upload_mimes', [ $this, 'svg_upload_allow' ] );
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_filter( 'wpcf7_spam', '__return_false' );
	}

	/**
	 * Add script and style.
	 *
	 * @return void
	 */
	public function add_scrip(): void {
		$url = get_template_directory_uri();

		wp_enqueue_style( 'slider', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', '', '1.8.1' );
		wp_enqueue_style( 'fancyapps', '//cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css', '', '4.0' );
		wp_enqueue_style( 'theme', $url . '/assets/css/slick-theme.css', '', self::T_THEMES_VERSION );
		wp_enqueue_style( 'style', $url . '/assets/css/style.css', '', self::T_THEMES_VERSION );

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//code.jquery.com/jquery-3.6.0.min.js', '', '3.6.0', false );

		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', [ 'jquery' ], '1.8.1', true );
		wp_enqueue_script( 'fancyapps', '//cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js', [ 'jquery' ], '4.0', true );
		wp_enqueue_script( 'script', $url . '/assets/js/app.min.js', [ 'jquery' ], self::T_THEMES_VERSION, true );
	}

	/**
	 * Add svg upload.
	 *
	 * @return array
	 */
	public function svg_upload_allow(): array {
		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Add theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {
		add_theme_support( 'post-tumbnails' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'custom-logo' );

		pll_register_string( 'my-string-button', 'Потурбуватись', 'Main Page' );

		pll_register_string( 'about-us', 'Про нас', 'Main Page' );
		pll_register_string( 'help', 'Кому ми допомагаємо', 'Main Page' );
		pll_register_string( 'photo-report', 'Звiти', 'Main Page' );
		pll_register_string( 'parthners', 'Партнери', 'Main Page' );

		pll_register_string( 'bank-card', 'Банкiвською картою', 'Main Page' );
		pll_register_string( 'number-grn', 'Номер рахунку Укр. в грн.', 'Main Page' );
		pll_register_string( 'number-swift', 'Номери рахунків євро та долар для SWIFT перекладів', 'Main Page' );
		pll_register_string( 'Etherium', 'Etherium', 'Main Page' );
		pll_register_string( 'PayPal', 'PayPal', 'Main Page' );
		pll_register_string( 'copied', 'Скопійовано', 'Main Page' );

		$this->add_new_table();
	}

	/**
	 * Create new table.
	 *
	 * @return void
	 */
	private function add_new_table(): void {
		$table_created = get_option( 'vpo_table_created', false );

		if ( $table_created ) {
			return;
		}

		global $wpdb;
		$table_name      = $wpdb->prefix . 'vpo';
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE `{$table_name}` 
				(
				    `id` BIGINT NOT NULL AUTO_INCREMENT , 
				    `name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL , 
				    `phone` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL , 
				    `date` DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)
				) 
    			$charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		dbDelta( $sql );

		update_option( 'vpo_table_created', true );
	}
}
