<?php
/**
 * Helpers class.
 *
 * @package iwpdev/turbota
 */

namespace IWPDEV\Turbota;

/**
 * Helpers class file.
 */
class Helpers {
	/**
	 * Helpers construct.
	 */
	public function __construct() {

	}

	/**
	 * Check valid user.
	 *
	 * @param string $phone Phone user number.
	 *
	 * @return bool
	 */
	public static function check_user( string $phone ): bool {
		global $wpdb;

		$table_name = $wpdb->prefix . 'vpo';
		// phpcs:disable
		$result = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT * FROM `{$table_name}` WHERE `phone` = %s",
				$phone
			),
			OBJECT
		);
		// phpcs:enable

		if ( ! empty( $result ) ) {
			$current_date = time();
			$time_request = strtotime( $result[0]->date . '30 days' );

			if ( $current_date > $time_request ) {
				return true;
			} else {
				return false;
			}
		}

		if ( empty( $result ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Add vpo user.
	 *
	 * @param string $name  User name.
	 * @param string $phone User phone.
	 *
	 * @return int
	 */
	public static function add_vpo_user( string $name, string $phone ): int {
		global $wpdb;

		$table_name = $wpdb->prefix . 'vpo';

		$wpdb->insert(
			$table_name,
			[
				'name'  => $name,
				'phone' => $phone,
			],
			[
				'%s',
				'%s',
			]
		);

		return $wpdb->insert_id;
	}
}
