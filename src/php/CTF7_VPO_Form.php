<?php
/**
 * Contact form 7 Handler Vpo form.
 *
 * @package iwpdev/turbota
 */

namespace IWPDEV\Turbota;

use WPCF7_ContactForm;
use WPCF7_Submission;

/**
 * CTF7_VPO_Form class file.
 */
class CTF7_VPO_Form {
	/**
	 * CTF7_VPO_Form construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hook and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wpcf7_before_send_mail', [ $this, 'before_send_email' ], 100 );
	}

	/**
	 * Handler form vpo.
	 *
	 * @param WPCF7_ContactForm $wpcf7 CF7 class.
	 *
	 * @return void
	 */
	public function before_send_email( WPCF7_ContactForm $wpcf7 ): void {
		$submission = WPCF7_Submission::get_instance();

		if ( 522 === $wpcf7->id() ) {
			$posted_data = $submission->get_posted_data();

			if ( empty( $posted_data ) ) {
				return;
			}

			if ( isset( $_COOKIE['vpo'] ) ) {
				die(
				json_encode(
					[
						'contact_form_id' => 522,
						'into'            => '#wpcf7-f522-o1',
						'invalid_fields'  => [],
						'message'         => __( 'Залишити заявку на допомогу можна заповнити один раз на місяць', 'turbota' ),
						'status'          => 'spam',
					]
				)
				);
			}

			$phone    = ! empty( $posted_data['phone'] ) ? filter_var( $posted_data['phone'], FILTER_SANITIZE_STRING ) : null;
			$vpo_name = ! empty( $posted_data['user_name'] ) ? filter_var( $posted_data['user_name'], FILTER_SANITIZE_STRING ) : '';

			if ( ! empty( $phone ) && Helpers::check_user( $phone ) ) {
				$vpo_id = Helpers::add_vpo_user( $vpo_name, $phone );
				setcookie( 'vpo', true, time() + 3600 * 24 * 30, COOKIEPATH, COOKIE_DOMAIN );
			}

		}
	}
}
