<?php
/**
 * Upload file in google drive.
 *
 * @package iwpdev/turbota
 */

namespace IWPDEV\Turbota;

use Google\Exception;
use Google\Service\Drive;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;

/**
 * GoogleDrive class file.
 */
class GoogleDrive {
	/**
	 * GoogleDrive construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and action.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'admin_head', [ $this, 'cron_init' ] );
		add_action( 'update_g_token', [ $this, 'update_token' ] );
		add_action( 'wpcf7_before_send_mail', [ $this, 'send_file_to_gdrive' ] );
	}

	/**
	 * Corn init.
	 *
	 * @return void
	 */
	public function cron_init(): void {
		if ( ! wp_next_scheduled( 'update_g_token' ) ) {
			wp_schedule_single_event( time() + 1800, 'update_g_token' );
		}
	}

	/**
	 * Cron update token.
	 * 30min cron jobs.
	 *
	 * @return void
	 * @throws Exception
	 */
	public function update_token(): void {
		$client = $this->get_client();
		if ( $client->isAccessTokenExpired() ) {
			if ( $client->getRefreshToken() ) {
				$client->fetchAccessTokenWithRefreshToken( $client->getRefreshToken() );
				update_option( 'it_google_token', $client->getAccessToken() );
			} else {
				$subject = 'IT-generation Google drive токен истек СУКА CRON WTF ! ';
				$to      = 'glazoknet@gmail.com';
				$message = 'Ну что пришло время поднимай задницу и обновляй токен тк крон вп не сработал и токен в проебе! А и да надеюсь ты знаешь что тебе нужно сделать. Если нет бегай по круки и кричи блядь все в проебе';
				$headers = [
					'From: it-generation <glazoknet@gmail.com>',
					'content-type: text/html',
					'cc: it-generation <skrynnyk.serhii@a-level.com.ua>',
				];

				wp_mail( $to, $subject, $message, $headers );
			}
		}
	}

	/**
	 * Send file to google drive.
	 *
	 * @param $cf7
	 *
	 * @throws \Google\Exception
	 */
	public function send_file_to_gdrive( $cf7 ) {
		if ( 522 === $cf7->id ) {
			$upload = wp_upload_dir();

			$uploads_dir = $upload['basedir'] . '/' . wpcf7_dnd_dir . '/';

			$user_name = ! empty( $_POST['user_name'] ) ? filter_var( wp_unslash( $_POST['user_name'] ), FILTER_SANITIZE_STRING ) : null;

			$all_files = array_merge(
				$_POST['user_wpo'] ?? [],
				$_POST['child_svidotsvo'] ?? [],
			);

			$phone = ! empty( $_POST['phone'] ) ? filter_var( $_POST['phone'], FILTER_SANITIZE_STRING ) : null;

			if ( ! Helpers::check_user( $phone ) || isset( $_COOKIE['vpo'] ) ) {
				return $cf7;
			}

			if ( 0 === count( $all_files ) || empty( $user_name ) ) {
				return $cf7;
			}

			$folder_id = $this->create_folder( $user_name );

			if ( $folder_id ) {
				foreach ( $all_files as $file ) {
					$file_name = explode( '/', $file )[1];
					$path      = $uploads_dir . $file;
					$mime_type = mime_content_type( $path );
					$result    = $this->insert_file_to_drive( $path, $file_name, $mime_type, $folder_id );
					unlink( $path );
				}
			}
		}

		return $cf7;
	}

	/**
	 * Create folder.
	 *
	 * @param string $folder_name folder name.
	 *
	 * @return false|mixed
	 * @throws \Google\Exception
	 */
	private function create_folder( string $folder_name ) {
		try {
			if ( $this->check_folder_exists( $folder_name ) ) {
				$client  = $this->get_client();
				$service = new Drive( $client );

				$folderName = 'form-downloads';
				$optParams  = [
					'pageSize' => 10,
					'fields'   => 'nextPageToken, files',
					'q'        => "name = '" . $folderName . "' and mimeType = 'application/vnd.google-apps.folder'",
				];

				$results = $service->files->listFiles( $optParams );


				foreach ( $results->getFiles() as $file ) {
					$parent_folder_id = $file->getId();
				}


				$service = new Google_Service_Drive( $client );

				$folder = new Google_Service_Drive_DriveFile();
				$folder->setParents( [ $parent_folder_id ] );
				$folder->setName( $folder_name );
				$folder->setMimeType( 'application/vnd.google-apps.folder' );

				$result = $service->files->create( $folder );

				if ( isset( $result['id'] ) && ! empty( $result['id'] ) ) {
					$folder_id = $result['id'];
				}

				return $folder_id;
			}
		} catch ( Exception $e ) {
			echo "Error Message: " . $e;
		}

		return false;
	}

	/**
	 * Insert file to google drive.
	 *
	 * @param string   $file_path File path.
	 * @param string   $file_name File name.
	 * @param string   $mime_type Mime type.
	 * @param int|null $folder_id folder id.
	 *
	 * @return bool
	 * @throws \Google\Exception
	 */
	private function insert_file_to_drive( $file_path, $file_name, $mime_type, $folder_id = null ) {
		$client  = $this->get_client();
		$service = new Google_Service_Drive( $client );
		$file    = new Google_Service_Drive_DriveFile();

		$file->setName( $file_name );

		if ( ! empty( $folder_id ) ) {
			$file->setParents( [ $folder_id ] );
		}

		$result = $service->files->create(
			$file,
			[
				'data'     => file_get_contents( $file_path ),
				'mimeType' => $mime_type,
			]
		);

		$is_success = false;

		if ( isset( $result['name'] ) && ! empty( $result['name'] ) ) {
			$is_success = true;
		}

		return $is_success;
	}

	/**
	 * Get Client.
	 *
	 * @return Google_Client
	 * @throws \Google\Exception
	 */
	private function get_client() {
		$client = new Google_Client();
		$client->setApplicationName( 'Google Drive API PHP Quickstart' );
		$client->setScopes( Google_Service_Drive::DRIVE_APPDATA );
		$client->setAuthConfig( get_stylesheet_directory() . '/src/cred/client_secret.json' );
		$client->setAccessType( 'offline' );
		$client->setPrompt( 'select_account consent' );

		$token = get_option( 'it_google_token', true );

		if ( ! empty( $token ) ) {
			$client->setAccessToken( $token );
		}

		// If there is no previous token or it's expired.
		if ( $client->isAccessTokenExpired() ) {
			// Refresh the token if possible, else fetch a new one.
			if ( $client->getRefreshToken() ) {
				$client->fetchAccessTokenWithRefreshToken( $client->getRefreshToken() );
				update_option( 'it_google_token', $client->getAccessToken() );
			} else {
				$subject = 'E-Turbota Google drive токен истек СУКА ! ';
				$to      = 'glazoknet@gmail.com';
				$message = 'токен:' . $client->getRefreshToken();
				$headers = [
					'From: it-generation <glazoknet@gmail.com>',
					'content-type: text/html',
				];

				wp_mail( $to, $subject, $message, $headers );
			}
		}

		return $client;
	}

	/**
	 * Check folder exists.
	 *
	 * @param string $folder_name folder name.
	 *
	 * @return bool
	 * @throws \Google\Exception
	 */
	private function check_folder_exists( $folder_name ) {

		$client  = $this->get_client();
		$service = new Google_Service_Drive( $client );

		$parameters['q'] = "mimeType = 'application/vnd.google-apps.folder' and name = '$folder_name' and trashed = false";
		$files           = $service->files->listFiles( $parameters );

		$op = [];
		foreach ( $files as $k => $file ) {
			$op[] = $file;
		}

		if ( 0 === count( $op ) ) {
			return true;
		}

		return false;
	}

}
