<?php
/**
 * Footer template.
 *
 * @package iwpdev/turbota
 */

?>
<footer class="footer">
	<div class="container">
		<div class="footer__row">
			<?php the_custom_logo(); ?>
			<div class="footer__column">
				<a class="footer__modal btn" href="#">
					<i class="icon svg-lesser-icon-dims">
						<svg>
							<use
									href="<?php echo esc_url( get_template_directory_uri() . '/assets/img/icons/icons.svg#lesser-icon' ); ?>"></use>
						</svg>
					</i>
					<?php echo esc_html( get_field( 'name-button-footer', get_option( 'page_on_front' ) ) ); ?>
				</a>
				<a
						class="footer__phone"
						href="tel:<?php echo esc_html( str_replace( ' ', '', get_field( 'phone', get_option( 'page_on_front' ) ) ) ); ?>">
					<?php echo esc_html( get_field( 'phone', get_option( 'page_on_front' ) ) ); ?>
				</a>
			</div>
		</div>

		<p class="footer__copyright"><?php the_field( 'copyright' ); ?></p>

	</div>
</footer>
</div>

<?php wp_footer(); ?>
</body>
</html>
