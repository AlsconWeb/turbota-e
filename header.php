<?php
/**
 * Header template.
 *
 * @package iwpdev/turbota
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="facebook-domain-verification" content="zxkxjlfijnykkkmb7ie280vr8t72r9"/>

	<?php wp_head(); ?>

</head>

<body class="body">
<div id="wrapper">

	<header class="header">
		<div class="container">
			<div class="header__column">

				<?php the_custom_logo(); ?>
				<a
						class="header__phone"
						href="tel:<?php echo esc_html( str_replace( ' ', '', get_field( 'phone' ) ) ); ?>">
					<?php the_field( 'phone' ); ?>
				</a>

				<ul class="header__lang-menu">
					<?php
					pll_the_languages(
						[
							'show_flags' => 1,
							'show_names' => 0,
						]
					);
					?>
				</ul>
			</div>
			<div class="header__column menu-mobile">
				<ul class="header__menu">
					<li class="header__item">
						<a href="#about-us">
							<?php echo esc_html( pll_e( 'Про нас' ) ); ?>
						</a>
					</li>
					<li class="header__item">
						<a href="#help">
							<?php echo esc_html( pll_e( 'Кому ми допомагаємо' ) ); ?>
						</a>
					</li>
					<li class="header__item">
						<a href="#photo-report">
							<?php echo esc_html( pll_e( 'Звiти' ) ); ?>
						</a>
					</li>
					<li class="header__item">
						<a href="#parthners">
							<?php echo esc_html( pll_e( 'Партнери' ) ); ?>
						</a>
					</li>
				</ul>
				<div class="header__button">
					<a class="btn" href="#requisites">
						<i class="icon svg-lesser-icon-dims">
							<svg>
								<use href="<?php bloginfo( 'template_url' ) . '/assets/img/icons/icons.svg#lesser-icon'; ?>"></use>
							</svg>
						</i>
						<?php echo esc_html( pll_e( 'Потурбуватись' ) ); ?>
					</a>
				</div>
			</div>

			<div class="toggle-menu">
				<i></i>
				<i></i>
				<i></i>
			</div>
		</div>
	</header>
