<?php
/**
 * Template Name: Callback.
 */

session_start();

$client = new Google\Client();
$client->setAuthConfigFile( get_stylesheet_directory() . '/src/cred/client_secret.json' );
$client->setRedirectUri( 'https://' . $_SERVER['HTTP_HOST'] . '/oauth2callback' );
$client->addScope( Google\Service\Drive::DRIVE );
$client->setAccessType( 'offline' );
$client->setPrompt( 'select_account consent' );

if ( ! isset( $_GET['code'] ) ) {
	$auth_url = $client->createAuthUrl();
	header( 'Location: ' . filter_var( $auth_url, FILTER_SANITIZE_URL ) );
} else {
	$client->authenticate( $_GET['code'] );
	update_option( 'it_google_token', $client->getAccessToken() );

	$redirect_uri = 'https://' . $_SERVER['HTTP_HOST'] . '/';
	header( 'Location: ' . filter_var( $redirect_uri, FILTER_SANITIZE_URL ) );
}