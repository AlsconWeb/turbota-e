<?php
/**
 * Index template.
 *
 * @package iwpdev/turbota
 */

get_header();
?>
	<section class="form-page">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 class="title"><?php the_title(); ?></h1>
					<?php echo do_shortcode( '[contact-form-7 id="522" title="заявки от ВПО" html_class=" form-wpo"]' ) ?>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
