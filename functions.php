<?php
/**
 * Theme functions file.
 *
 * @package iwpdev/turbota
 */

use IWPDEV\Turbota\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();
