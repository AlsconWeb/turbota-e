<?php
/**
 * Template Name: home
 *
 * @package iwpdev/turbota
 */

get_header();
?>

	<div id="contactUs" class="popup-wrap">
		<div class="content-block">
			<div class="close-popup"></div>
			<?php echo do_shortcode( '[contact-form-7 id="14" title="Contact form"]' ); ?>
		</div>
	</div>


	<main id="main">
		<section class="hero">
			<div class="slider-for">
				<?php

				$arg_slider = [
					'posts_per_page' => 5,
					'category_name'  => 'custom-slider',
					'post_type'      => 'post',
					'order'          => 'ASC',
				];

				$query_slider = new WP_Query( $arg_slider );

				if ( $query_slider->have_posts() ) {
					while ( $query_slider->have_posts() ) {
						$query_slider->the_post();
						?>
						<div class="slide-container">

							<?php
							$image = get_field( 'help-pict-we' );
							if ( ! empty( $image ) ) {
								?>
								<img
										src="<?php echo esc_url( $image['url'] ); ?>" width="1280" height="418"
										alt="<?php echo esc_attr( $image['alt'] ); ?>"/>
							<?php } ?>
							<div class="slider-content">
								<h1><?php the_title(); ?></h1>
								<p><?php the_field( 'help-desc-we' ); ?></p>
								<div class="header__button">
									<a class="btn" href="#requisites">
										<i class="icon svg-lesser-icon-dims">
											<svg>
												<use
														href="<?php echo esc_url( get_template_directory_uri() . '/assets/img/icons/icons.svg#lesser-icon' ); ?>"></use>
											</svg>
										</i><?php the_field( 'slider-button-name' ); ?></a>
								</div>
							</div>

						</div>
						<?php
					}
					wp_reset_postdata();
				}
				?>
			</div>
		</section>

		<section id="about-us" class="about-us">
			<div class="container">
				<div class="about-us__content">
					<h2 class="about-us__title">
						<?php the_field( 'about-us-title' ); ?>
					</h2>
					<?php the_field( 'about-us-desc' ); ?>

				</div>
				<div class="about-us__galery">
					<?php
					$image = get_field( 'about-us-picture' );
					if ( ! empty( $image ) ) {
						?>
						<div class="about-us__picture">
							<img
									src="<?php echo esc_url( $image['url'] ); ?>" width="302" height="182"
									alt="<?php echo esc_attr( $image['alt'] ); ?>"/>
						</div>
					<?php } ?>

					<?php
					$image = get_field( 'about-us-picture-1' );
					if ( ! empty( $image ) ) {
						?>
						<div class="about-us__picture">
							<img
									src="<?php echo esc_url( $image['url'] ); ?>" width="302" height="182"
									alt="<?php echo esc_attr( $image['alt'] ); ?>"/>
						</div>
					<?php } ?>

					<?php
					$image = get_field( 'about-us-picture-2' );
					if ( ! empty( $image ) ) {
						?>
						<div class="about-us__picture">
							<img
									src="<?php echo esc_url( $image['url'] ); ?>" width="302" height="182"
									alt="<?php echo esc_attr( $image['alt'] ); ?>"/>
						</div>
					<?php } ?>

				</div>
			</div>
		</section>

		<section id="help" class="help">
			<div class="container">
				<h2 class="help__title">
					<?php the_field( 'who-help-we-title' ); ?>
				</h2>

				<div class="help__gallery">
					<?php

					$arg_help    = [
						'posts_per_page' => 5,
						'category_name'  => 'work-id',
						'post_type'      => 'post',
					];
					$query_helps = new WP_Query( $arg_help );

					if ( $query_helps->have_posts() ) {
						while ( $query_helps->have_posts() ) {
							$query_helps->the_post();
							?>
							<div class="help__block">
								<?php
								$image = get_field( 'help-pict-we' );
								if ( ! empty( $image ) ) {
									?>
									<img
											src="<?php echo esc_url( $image['url'] ); ?>" width="410" height="278"
											alt="<?php echo esc_attr( $image['alt'] ); ?>"/>
								<?php } ?>

								<div class="help__item">
									<h3 class="help__item-title"><?php the_title(); ?></h3>
									<p class="help__item-desc"><?php the_field( 'help-desc-we' ); ?></p>
								</div>
								<div class="help__long-desc">
									<?php the_content(); ?>
									<div class="header__button">
										<a class="btn" href="#requisites">
											<i class="icon svg-lesser-icon-dims">
												<svg>
													<use
															href="<?php echo esc_url( get_template_directory_uri() . '/assets/img/icons/icons.svg#lesser-icon' ); ?>"></use>
												</svg>
											</i>
											<?php echo esc_html( pll_e( 'Потурбуватись' ) ); ?>
										</a>
									</div>
								</div>
								<div class="help__shadow"></div>
							</div>
							<?php
						}
						wp_reset_postdata();
					}
					?>
				</div>
			</div>
		</section>

		<section id="photo-report" class="photo-report">
			<div class="container">
				<h2 class="photo-report__title">
					<?php the_field( 'photo-video-title' ); ?>
				</h2>
				<div id="mainCarousel" class="photo-report__carusel custom-carusel">
					<?php echo do_shortcode( '[sp_wpcarousel id="496"]' ); ?>
				</div>
			</div>
		</section>

		<section class="photo-report">
			<div class="container">
				<h2 class="photo-report__title">
					<?php the_field( 'video-carusel-title' ); ?>
				</h2>
				<div class="photo-report__carusel">
					<?php echo do_shortcode( '[new_royalslider id="1"]' ); ?>
				</div>
			</div>
		</section>

		<section id="requisites" class="requisites">
			<div class="container">
				<h2 class="requisites__title"><?php the_field( 'title-help' ); ?></h2>
				<div class="slider-arrows">
					<button id="prevArrow" class="prev-arrow arrow-pay">
						<i class="fa fa-angle-left"></i>
					</button>
					<button id="nextArrow" class="next-arrow arrow-pay">
						<i class="fa fa-angle-right"></i>
					</button>
				</div>
				<div class="requisites__row">
					<div class="requisites__bank" id="slider">
						<ul class="nav">
							<li class="active">
								<a href="step-one" class="btn-tab">
									<?php echo esc_html( pll_e( 'Банкiвською картою' ) ); ?>
								</a>
							</li>
							<li>
								<a href="step-two" class="btn-tab">
									<?php echo esc_html( pll_e( 'Номер рахунку Укр. в грн.' ) ); ?>
								</a>
							</li>
							<li>
								<a href="step-three" class="btn-tab">
									<?php echo esc_html( pll_e( 'Номери рахунків євро та долар для SWIFT перекладів' ) ); ?>
								</a>
							</li>
							<li>
								<a href="step-four" class="btn-tab">
									<?php echo esc_html( pll_e( 'Etherium' ) ); ?>
								</a>
							</li>
							<li>
								<a href="step-five" class="btn-tab">
									<?php echo esc_html( pll_e( 'PayPal' ) ); ?>
								</a>
							</li>
						</ul>
					</div>

					<div id="step-one" class="table">
						<h3 class="table__title"><?php the_field( 'name-bank-title' ); ?></h3>
						<p class="table__desc"><?php the_field( 'name-bank-desc' ); ?></p>

						<div class="table__card">
							<img
									class="bank-picture"
									src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/monobank.png' ); ?>"
									alt="Monobank" width="160" height="58"/>
							<div class="copy-row-number">
								<code class="bank-number content">
									<?php the_field( 'monobank-number' ); ?>
								</code>
							</div>
						</div>

						<div class="table__card">
							<img
									class="bank-picture"
									src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/uksibbank.png' ); ?>"
									alt="Ukrsibbank" width="160" height="53"/>
							<div class="copy-row-number">
								<code class="bank-number content">
									<?php the_field( 'number-ukrsibbank' ); ?>
								</code>
							</div>
						</div>

					</div>
					<div id="step-two" class="table hide ukraine-grn">
						<h3 class="table__title"><?php the_field( 'title-grn' ); ?></h3>
						<p class="table__desc"><?php the_field( 'desc-grn' ); ?></p>
						<?php the_field( 'rahunok-grn-info' ); ?>
						<div class="copy-row-number">
							<code class="bank-number content">
								<?php the_field( 'number-grn' ); ?>
							</code>
						</div>
					</div>

					<div id="step-three" class="table hide swift">
						<h3 class="table__title"><?php the_field( 'title-swift' ); ?></h3>
						<p class="table__desc"><?php the_field( 'desc-swift' ); ?></p>
						<?php the_field( 'swift-usd-euro' ); ?>
					</div>

					<div id="step-four" class="table hide">
						<h3 class="table__title"><?php the_field( 'etherium-title' ); ?></h3>
						<p class="table__desc"><?php the_field( 'etherium_опис' ); ?></p>
						<?php
						$image = get_field( 'qr_code' );
						if ( ! empty( $image ) ) {
							?>
							<img
									class="qr-code" src="<?php echo esc_url( $image['url'] ); ?>" width="237"
									height="242"
									alt="<?php echo esc_attr( $image['alt'] ); ?>"/>
						<?php } ?>

						<div class="copy-row-number">
							<code class="bank-number content">
								<?php the_field( 'etherium-number' ); ?>
							</code>
						</div>
					</div>

					<div id="step-five" class="table hide">
						<h3 class="table__title"><?php the_field( 'paypal-title' ); ?></h3>
						<p class="table__desc"><?php the_field( 'paypal-desc' ); ?></p>
						<div class="table__card">
							<img
									class="paypal-logo"
									src="<?php bloginfo( 'template_url' ) . '/assets/img/paypal.png'; ?>"
									width="200" height="53" alt="PayPal"/>
							<div class="copy-row-number">
								<code class="bank-number content">
									<?php the_field( 'paypal-number' ); ?>
								</code>
							</div>
						</div>

					</div>
					<div class="tooltip" id="myTooltip">
						<?php echo esc_html( pll_e( 'Скопійовано' ) ); ?>
					</div>
				</div>
			</div>
		</section>

		<section id="off-document" class="off-document">
			<div class="container">
				<h2 class="off-document__title">
					<?php the_field( 'reg-document-title' ); ?>
				</h2>
				<div class="off-document__lightbox">
					<?php echo do_shortcode( '[wonderplugin_carousel id="2"]' ); ?>
				</div>
			</div>
		</section>

		<section id="report" class="report">
			<div class="container">
				<h2 class="report__title">
					<?php the_field( 'title-report-doc' ); ?>
				</h2>
				<ul class="report__list">

					<?php
					global $item_post;

					$myposts = get_posts(
						[
							'posts_per_page' => 6,
							'category_name'  => 'document-id',
							'post_type'      => 'post',
						]
					);

					foreach ( $myposts as $item_post ) {
						setup_postdata( $item_post );
						?>
						<li class="report__item">
							<i class="icon svg-file-icon-dims">
								<svg>
									<use
											href="<?php bloginfo( 'template_url' ); ?>/assets/img/icons/icons.svg#file-icon"></use>
								</svg>
							</i>
							<?php
							$file = get_field( 'add-file-pdf' );
							if ( $file ) {
								?>
								<a
										href="<?php echo esc_url( $file['url'] ); ?>"
										target="_blank">
									<?php echo esc_html( $file['filename'] ); ?>
								</a>
							<?php } ?>
						</li>
						<?php
					}
					wp_reset_postdata();
					?>
				</ul>
			</div>
		</section>
		<?php
		if ( get_field( 'showhide' ) ) {
			?>
			<section id="parthners" class="parthners">
				<div class="container">
					<h2 class="parthners__title">
						<?php the_field( 'tittle-parthner' ); ?>
					</h2>
					<div class="parthners__corusel">
						<?php echo do_shortcode( '[sp_wpcarousel id="35"]' ); ?>
					</div>
				</div>
			</section>
			<?php
		}
		?>

	</main>

<?php
get_footer();
